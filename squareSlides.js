(function( $ ) {
  
    var methods = {
    init : function( options ) { 
      // THIS 
    },
    show : function( ) {
      // IS
    },
    hide : function( ) { 
      // GOOD
     },
    next : function( squareSlides, settings, i ) {
      var div_width = (squareSlides.width() / settings.num_squares_x);
      var div_height = (squareSlides.height() / settings.num_squares_y);
      var col, row = 0;
      var ss_index = squareSlides.data('ss_index');
      var bgurl = "url(" + settings.images[ss_index] + ")";
      squareSlides.data('ss_index', (ss_index+1) % settings.images.length);
      squareSlides.children().each(function(index, elem) {
          var random = Math.random() * (settings.transition_time * 1000);
          if (index % settings.num_squares_x == 0) {
              col = 0;
              if (index != 0) {
                  row++;   
              }
          }
          var x = '-' + (col*div_width) + 'px';
          var y = '-' + (row * div_height) + 'px';
          var cssObj = {
            'background':bgurl,
            'backgroundPosition': (x + ' ' + y)
          };
          setTimeout(function() { methods.singleNext($(elem).children(), cssObj, settings.effect, settings.effect_time); }, settings.interval + random);
          col++;
      });
    
    },
    singleNext : function (elem, cssObj, effect, effect_time) {
      //do whatever transition effects
      if (effect == 'fade') {
        var firstChild = $(elem[0]);
        var w =  firstChild.width();
        firstChild.animate({
          left  : 45 + '%',
          width : 10 + '%',
          }, effect_time/2, 'linear', function() {
            $(this).css(cssObj);
            $(this).animate({
              left  : 0 + 'px',
              width : 100 + '%'
              }, effect_time/2, 'linear');
          });
      }
      else {
        elem.css(cssObj); 
      }
    }
  };

  $.fn.squareSlides = function( options) {
    
     var settings = $.extend( {
      'interval'        : 5, //seconds
      'transition_time' : 1.5, //seconds
      'num_squares_x'   : 3,
      'num_squares_y'   : 3,
      'images'          : [],
      'effect'          : '',
      'effect_time'     : .5, //seconds
      'startingImage'   : ''
    }, options);
     
     settings.effect_time = settings.effect_time * 1000;

    $this = this;
    $this.data('ss_index', 0);
    if (settings.images.length == 0) {
      $.error('No image urls supplied for squareSlides');
    }
    if (settings.transition_time >= settings.interval) {
      $.error('Transition time must be less than the interval between images.');
    }
    
    //create internal elements
    $this.children().each(function() {
      $(this).css('position', 'relative');
      var actual = $('<div></div>');
      actual.css({'position':'absolute', 'width':$(this).width(), 'height':$(this).height(), 'left':0, 'top':0});
      $(this).append(actual);
    });
    if (settings.startingImage.length != 0) {
      methods.next($this, settings);
    }
    setInterval( function() { methods.next($this, settings) }, settings.interval * 1000 );

    return this;

  };
})( jQuery );